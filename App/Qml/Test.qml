import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Controls.Material

Button {
    objectName: "btnClickMe"
    text: "Click Me"
    onClicked: print("Clicked!")
}

