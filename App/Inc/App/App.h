#pragma once
#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlEngine>

class App : public QObject {
    Q_OBJECT

public:
    App();
    Q_INVOKABLE QString test();

    const QQmlApplicationEngine& engine() const;

private:
    QQmlApplicationEngine m_engine;
};
