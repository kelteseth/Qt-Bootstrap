#include "App/App.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml/qqmlextensionplugin.h>

Q_IMPORT_QML_PLUGIN(AppPlugin)

int main(int argc, char* argv[])
{
    QGuiApplication::setOrganizationName("AppOrganization");
    QGuiApplication::setOrganizationDomain("www.example.app");
    QGuiApplication::setApplicationName("App");
    QGuiApplication::setApplicationVersion("1.0.0");

    QGuiApplication guiApp(argc, argv);
    App app;
    return guiApp.exec();
}
