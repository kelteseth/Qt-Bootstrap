#include "App/App.h"

#include <QCoreApplication>
#include <QDebug>
#include <QString>

App::App()
{
    auto* app = QCoreApplication::instance();
    // We put the plugins in the qml/ subdir
    // See CMake: OUTPUT_DIRECTORY ${APP_QML_MODULES_PATH}/${PROJECT_NAME}
    m_engine.addImportPath(app->applicationDirPath() + "/qml");
    app->addLibraryPath(app->applicationDirPath() + "/qml");
#if defined(Q_OS_OSX)
    QDir workingDir(guiApplication->applicationDirPath());
    workingDir.cdUp();
    workingDir.cdUp();
    workingDir.cdUp();
    // OSX Development workaround:
    m_mainWindowEngine->addImportPath(workingDir.path() + "/qml");
#endif

    qmlRegisterSingletonInstance("App", 1, 0, "App", this);

    // The first subfolder is the libraryName followed by the regular
    // folder strucutre:     LibararyName/Subfolder
    const QUrl url(u"qrc:/App/Main.qml"_qs);
    //    QObject::connect(&m_engine, &QQmlApplicationEngine::objectCreated,
    //        &app, [url](QObject *obj, const QUrl &objUrl) {
    //            if (!obj && url == objUrl)
    //                QCoreApplication::exit(-1);
    //        }, Qt::QueuedConnection);
    m_engine.load(url);
}

QString App::test()
{
    return "World";
}

const QQmlApplicationEngine& App::engine() const
{
    return m_engine;
}
