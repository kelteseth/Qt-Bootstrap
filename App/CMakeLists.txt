cmake_minimum_required(VERSION 3.23)

project(
    App
    VERSION 0.1
    LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(
    Qt6
    COMPONENTS Core Gui Quick Test
    REQUIRED)

set(QML # cmake-format: sortable
        Main.qml Qml/Test.qml)

set(QML_PLUGIN_SOURCES # cmake-format: sortable
                       Src/App.cpp)

set(QML_PLUGIN_HEADER # cmake-format: sortable
                      Inc/App/App.h)

qt_add_library(${PROJECT_NAME}Lib STATIC)
qt_add_qml_module(
    ${PROJECT_NAME}Lib
    OUTPUT_DIRECTORY
    ${APP_QML_MODULES_PATH}/${PROJECT_NAME}
    URI
    ${PROJECT_NAME} # QML: import App 1.0
    VERSION
    1.0
    QML_FILES
    ${QML}
    SOURCES
    ${QML_PLUGIN_SOURCES}
    ${QML_PLUGIN_HEADER})

target_include_directories(AppLib PRIVATE ./Inc ./Inc/App)
target_include_directories(AppLibplugin PUBLIC ./Inc ./Inc/App)

# Qt6 Generates an additional static library called ${PROJECT_NAME}plugin. <- Plugin LOWER casing All users of this must also call
# Q_IMPORT_QML_PLUGIN in the main.cpp between the includes and main:
#
# #include <QtQml/qqmlextensionplugin.h> Q_IMPORT_QML_PLUGIN(${PROJECT_NAME}Plugin) <- Plugin UPPER casing
#
# See: https://www.qt.io/blog/qml-modules-in-qt-6.2
qt_add_executable(App main.cpp)
target_link_libraries(App PRIVATE Qt6::Quick Qt6::Gui AppLibplugin)
