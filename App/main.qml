import QtQuick
import QtQuick.Window
import App

Window {
    id:root
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World App")

    Component.onCompleted: {
        print("Hello " + App.test())
    }

    Test {
        anchors.centerIn: parent
    }

}
