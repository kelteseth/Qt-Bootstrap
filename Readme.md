
<div align="center">
<img width="300" height="249" src="/App/Assets/Qt-Boostrap.png">

<h1>Qt 6 Example Application</h1>

[![pipeline status](https://gitlab.com/kelteseth/Qt-Bootstrap/badges/master/pipeline.svg)](https://gitlab.com/kelteseth/Qt-Bootstrap/commits/master)
[![coverage report](https://gitlab.com/kelteseth/Qt-Bootstrap/badges/master/coverage.svg)](https://gitlab.com/kelteseth/Qt-Bootstrap/commits/master)
[![license-MIT-green](https://img.shields.io/badge/license-MIT-green)](https://en.wikipedia.org/wiki/MIT_License)

</div>
<div align="center">

[![Documentation](https://img.shields.io/badge/docs-QDoc-blue.svg)](https://gitlab.com/kelteseth/Qt-Bootstrap)
[![Package Manager](https://img.shields.io/badge/Package_Manager-Vcpkg-blue.svg)](https://github.com/microsoft/vcpkg)
[![Code Formatting](https://img.shields.io/badge/Code_Formatting-Clang_Format-blue.svg)](https://www.qt.io/blog/2019/04/17/clangformat-plugin-qt-creator-4-9)
[![Static Analyser](https://img.shields.io/badge/Static_Analyser-Clang_Tidy_Clazy-blue.svg)](https://doc.qt.io/qtcreator/creator-clang-tools.html)
</div>

# Getting started

### Basic
1. Install latest git
2. Download Qt Bootstrap
``` bash
# Download into two subfolder so we have our build folder inside of the first folder
git clone --recursive https://gitlab.com/kelteseth/Qt-Bootstrap.git Qt-Bootstrap/Qt-Bootstrap
```
3. Run `pyton setup.py` in the `Tools` directory. This will download `Qt` and tools like `vcpkg` and `QtCreator` without the need of an `Qt` account!
4. Open QtCreator provided by `aqt`
5. Open the `CMakeLists.txt` and select the CMake preset from the `CMakePresets.json`
6. Change the name App to your liking
7. Optional:
    - Add `vcpkg` dependencies or change the version in the `Tools/defines.py`
    - Change `Qt` version in the `Tools/defines.py` 

# Current feature list

<div align="center">

| Feature                	|Type       | Windows 	| Linux 	| MacOS 	|
|------------------------	|---------	|---------	|-------	|-------	|
| __Testing__              	| QTest | ✔       	| ✔     	| ✔     	|
| __Continuous Integration__   |Gitlab CI  | ✔        	|  ❓     	|   ❌    Help Needed!	|
| __Documentation Generation__                  	|QDocs      | ✔        	|  ✔     	|  ✔     	|
| __Static Analyser__       	|Clang-Tidy & Clazy| ✔        	| ✔     	|     ✔  	|
| __Code Formatting__       	|Clang-Format|  ✔       	|  ✔     	| ✔      	|
| __Dependency Management__    |VCPKG| ✔        	|    ✔   	|  ✔     	|
| __Multilanguage__    | Internationalization with Qt| ✔        	|    ✔   	|  ✔     	|

</div>


### Continuous Integration (Gitlab)
You need to have  a gitlab runner to use this feature. Ether get a free one from gitlab.com (Only Linux) or setup one youself in 5minutes 
1. [Install a gitlab runner](https://docs.gitlab.com/runner/#install-gitlab-runner)
2. [Register your runner](https://docs.gitlab.com/runner/register/index.html)

3. Example: Register on a windows device at home:
   * ./gitlab-runner.exe register
   * Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com ) -> https://gitlab.com
   * Please enter the gitlab-ci token for this runner -> YourSecretToken via your project (or group if you want to allow all projects inside a group to use this runner) 
      * Project -> Settings -> CI / CD -> Runners -> Set up a specific Runner manually
   * Please enter the gitlab-ci description for this runner -> [hostname] my-windows-runner
   * Use vs2017 and windows as tags on a windows machine.
   * Use shell as executor 

### Docs (QDocs)
QDocs is the official way to generate Docs for you C++ and QML Code. Bootstrap and a custom CSS file is included if you want to change the look and feel.
* Example windows:
 * C:\Qt\5.14.0\msvc2017_64\bin\qdoc.exe D:\Code\Qt\ScreenPlay\Docs\config.qdocconf

* Tipp: You can use [GitLabs Mermaid to draw diagrams in your markdown](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts-using-mermaid) 

### Static Analyser (Clang-Tidy & Clazy)
Clang-Tidy & Clazy are used via the QtCreator debug section.

### Code Formatting (Clang-Format)
Clang Format is to be used with QtCreator. The default is based on the Webkit style because it matches the closed with the Qt style. You can change it via [clang format configurator](https://zed0.co.uk/clang-format-configurator/) to your liking!

### Dependency Management (VCPKG)
> Vcpkg helps you manage C and C++ libraries on Windows, Linux and MacOS. This tool and ecosystem are constantly evolving; your involvement is vital to its success!

Simply put your desired dependency into the vcpkg.json

* [Browse avilable libraries](https://vcpkg.io/en/packages.html)
