import os
import subprocess
import argparse
import util

stdout.reconfigure(encoding='utf-8')

def format_file_function(file):
    executable = "clang-format"
    if os.name == 'nt':
        executable = "clang-format.exe"
    process = subprocess.run(" %s -style=file -i %s" %
                             (executable, file), capture_output=True, shell=True)
    print("Format: %s \t return: %s" % (file, process.returncode))


def main(git_staged_only=False):
    file_list = util.find_files(('.cpp', '.h'), "", git_staged_only)
    util.execute_threaded(file_list, format_file_function)
    if not git_staged_only:
        util.check_git_exit("Clang Format")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', action="store_true", dest="stage_only",
                        default=False)
    args = parser.parse_args()
    main(args.stage_only)
