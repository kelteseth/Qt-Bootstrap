import os
import argparse
import util
from sys import stdout

stdout.reconfigure(encoding='utf-8')

def format_file_function(file):
    executable = "cmake-format"
    if os.name == 'nt':
        executable = "cmake-format.exe"
    os.system(" %s -c ../.cmake-format.py -i %s" % (executable, file))
    print("Format: ", file)


def main(git_staged_only=False):
    file_list = util.find_files(('CMakeLists.txt'), "", git_staged_only)
    util.execute_threaded(file_list, format_file_function)
    if not git_staged_only:
        util.check_git_exit("CMake Format")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', action="store_true", dest="stage_only",
                        default=False)
    args = parser.parse_args()
    main(args.stage_only)
