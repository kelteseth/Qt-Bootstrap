import subprocess
import sys
import os
from multiprocessing import cpu_count
from multiprocessing import Pool
from pathlib import Path
from os import chdir
from concurrent.futures import ThreadPoolExecutor
import os
import subprocess
from sys import stdout

stdout.reconfigure(encoding='utf-8')


def printRed(skk): print("\033[91m {}\033[0;37;40m" .format(skk))
def printYellow(skk): print("\033[93m {}\033[0;37;40m" .format(skk))

def execute(command, workingDir=".", ignore_error=False, use_shell=True, print_command=True):

    if print_command:
        print("\033[92m Executing: \033[0;37;40m", command)

    process = subprocess.Popen(
        command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=use_shell, cwd=workingDir)

    while True:
        if process.poll() is not None:
            break
        byte_line = process.stdout.readline()
        text_line = byte_line.decode('utf8', errors='ignore')
        if text_line:
            if ' warning' in text_line.lower():
                printYellow(text_line.strip())
            elif ' error' in text_line.lower():
                printRed(text_line.strip())
            else:
                print(text_line.strip())

    process.communicate()
    exitCode = process.returncode
    if exitCode:
        if ignore_error:
            printYellow("Ignore error {}".format(exitCode))
        else:
            raise subprocess.CalledProcessError(exitCode, command)

def run(cmd, cwd=Path.cwd()):
    result = subprocess.run(cmd, shell=True, cwd=cwd)
    if result.returncode != 0:
        raise RuntimeError(f"Failed to execute {cmd}")

def run_and_capture_output(cmd, cwd=Path.cwd()) -> str:
    result = subprocess.run(cmd, shell=True, cwd=cwd, stdout=subprocess.PIPE)
    if result.returncode != 0:
        print(f"Failed to execute {cmd}")
    if result.stdout is not None:
        return str(result.stdout.decode('utf-8'))
    return ""

def cd_repo_root_path() -> str:
    # Make sure the script is always started from the same 
    # ScreenPlay root folder
    root_path = Path.cwd()
    if root_path.name == "Tools":
        root_path = root_path.parent
        print(f"Change root directory to: {root_path}")
        chdir(root_path)
    return root_path

def run_io_tasks_in_parallel(tasks):
    with ThreadPoolExecutor() as executor:
        running_tasks = [executor.submit(task) for task in tasks]
        for running_task in running_tasks:
            running_task.result()

# Based on https://gist.github.com/l2m2/0d3146c53c767841c6ba8c4edbeb4c2c


def get_vs_env_dict():
    vcvars: str  # We support 2019 or 2022

    # Hardcoded VS path
    # check if vcvars64.bat is available.
    msvc_2019_path = "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\VC\\Auxiliary\\Build\\vcvars64.bat"
    msvc_2022_path = "C:\\Program Files\\Microsoft Visual Studio\\2022\\Community\\VC\\Auxiliary\\Build\\vcvars64.bat"

    if Path(msvc_2019_path).exists():
        vcvars = msvc_2019_path
    # Prefer newer MSVC and override if exists
    if Path(msvc_2022_path).exists():
        vcvars = msvc_2022_path
    if not vcvars:
        raise RuntimeError(
            "No Visual Studio installation found, only 2019 and 2022 are supported.")

    print(f"\n\nLoading MSVC env variables via {vcvars}\n\n")

    cmd = [vcvars, '&&', 'set']
    popen = subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = popen.communicate()

    if popen.wait() != 0:
        raise ValueError(stderr.decode("mbcs"))
    output = stdout.decode("mbcs").split("\r\n")
    return dict((e[0].upper(), e[1]) for e in [p.rstrip().split("=", 1) for p in output] if len(e) == 2)


def find_all_files(path):
    file_list = []
    for root, dirnames, files in os.walk(path):
        for filename in files:
            file_list.append(os.path.join(root, filename))
    return file_list


def find_all_git_staged_files():
    process = subprocess.run("git diff --name-only --staged",
                             capture_output=True, shell=True)
    out = process.stdout.decode("utf-8")
    out = out.splitlines()
    return out


def find_files(file_endings_tuple, path="", git_staged_only=False):
    file_list = []
    # Get the root folder by moving one up
    root = Path(__file__).parent.absolute()
    root = os.path.abspath(os.path.join(root, "../"))

    root = os.path.join(root, path)
    print(root)

    if git_staged_only:
        file_list = find_all_git_staged_files()
    else:
        file_list = find_all_files(root)

    filtered_file_list = []
    for filename in file_list:
        if filename.endswith(file_endings_tuple):
            filtered_file_list.append(os.path.join(root, filename))

    return filtered_file_list


def execute_threaded(file_list, format_file_function):
    p = Pool(cpu_count())
    p.map(format_file_function, file_list)
    p.close()
    p.join()


def check_git_exit(caller_name):
    # Check if all files are formatter
    process = subprocess.run("git --no-pager diff",
                             capture_output=True, shell=True)

    out = process.stdout.decode("utf-8")

    if out != "":
        print("\n########### %s DONE ###########\n" % caller_name)
        print("Git diff is not empty. This means your files where not correctly formatted!")
        out.replace("\\n", "\n")
        # print(out)
        sys.exit(1)
