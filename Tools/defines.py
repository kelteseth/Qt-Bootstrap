import sys
from pathlib import Path
from sys import stdout

stdout.reconfigure(encoding='utf-8')

APP_VERSION = "0.1.0"
# Master 11.11.2022
# See: https://github.com/microsoft/vcpkg
VCPKG_VERSION = "2871ddd"
# See: https://vcpkg.io/en/packages.html
VCPKG_PACKAGES = [
    "fmt"
]
# Qt Version _must_ also be changed in the CMakePresets.json!
QT_VERSION = "6.4.1"
QT_TARGET_PLATFORM = "desktop"
QT_IFW_VERSION = "4.5"
PYTHON_EXECUTABLE = "python" if sys.platform == "win32" else "python3"
AQT_PATH = Path("C:/aqt") if sys.platform == "win32" else Path().home().joinpath("aqt")
# aqt list-qt windows desktop --modules 6.3.2 win64_msvc2019_64
QT_MODULES = "qt3d qtquick3d qt5compat qtimageformats qtmultimedia qtshadertools qtwebchannel qtwebengine qtwebsockets qtwebview qtpositioning"
# see: aqt list-tool windows desktop
QT_TOOLS = ["tools_ifw", "tools_qtcreator", "tools_ninja"]

# Defined by Qt
if sys.platform == "win32":
    OS = "windows"
    QT_PLATFORM = "win64_msvc2019_64" # Note this is not the folder name! See `python -m aqt list-qt windows desktop --arch 6.4.1`
elif sys.platform == "darwin":
    OS = "mac"
    QT_PLATFORM = "macos"
elif sys.platform == "linux":
    OS = "linux"
    QT_PLATFORM = "gcc_64"
