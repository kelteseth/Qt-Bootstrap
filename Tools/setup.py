#!/usr/bin/python3

from platform import system
from pathlib import Path
import defines
import argparse
import util
import defines
import datetime
from sys import stdout

stdout.reconfigure(encoding='utf-8')


class commands_list():
    def __init__(self):
        self.commands = []

    def add(self, command, cwd=".", ignore_error=False, use_shell=True, print_command=True):
        self.commands.append({
            "command": command,
            "cwd": cwd,
            "ignore_error": ignore_error,
            "use_shell": use_shell,
            "print_command": print_command
        })

    def get_commands(self):
        return self.commands

    def execute_commands(self):
        '''
        This function execute all commands added to the list.
        '''
        for command in self.commands:
            # Check if the command if a string.
            if isinstance(command["command"], str):
                util.execute(command["command"], command["cwd"], command["ignore_error"],
                        command["use_shell"], command["print_command"])
            else:
                # Function call
                command["command"]()


def download_qt():

    print(f"Downloading: {defines.QT_MODULES} to {defines.AQT_PATH}")
    util.execute(f"{defines.PYTHON_EXECUTABLE} -m aqt install-qt -O {defines.AQT_PATH} {defines.OS} {defines.QT_TARGET_PLATFORM} {defines.QT_VERSION} {defines.QT_PLATFORM} -m {defines.QT_MODULES}")

    # Tools can only be installed one at the time:
    for tool in defines.QT_TOOLS:
        util.execute(
            f"{defines.PYTHON_EXECUTABLE} -m aqt install-tool -O {defines.AQT_PATH} {defines.OS} desktop {tool}")


def setup_qt():
    print(f"Setup Qt via aqt at {defines.AQT_PATH}")

    if not defines.AQT_PATH.exists():
        download_qt()
    else:
        # Betas & RCs are technically the same version. So limit download to x days
        days = 30
        aqt_path = defines.AQT_PATH
        qt_base_path = aqt_path.joinpath(defines.QT_VERSION).resolve()
        folder_creation_date: datetime = datetime.datetime.fromtimestamp(
            qt_base_path.stat().st_mtime, tz=datetime.timezone.utc)
        now: datetime = datetime.datetime.now(tz=datetime.timezone.utc)
        two_weeks_ago: datetime = now - datetime.timedelta(days=days)
        if (folder_creation_date < two_weeks_ago):
            print(f"qt version at `{qt_base_path}` older than {days} days ({folder_creation_date}), redownload!")
            download_qt()
        else:
            print(f"Qt {defines.QT_VERSION} is up to date and ready ")


def main():
    parser = argparse.ArgumentParser(
        description='Build and Package ScreenPlay')
    parser.add_argument('-skip-aqt', action="store_true", dest="skip_aqt",
                        help="Downloads QtCreator and needed binaries \Windows: C:\aqt\nLinux & macOS:~/aqt/.")
    args = parser.parse_args()

    root_path = Path(util.cd_repo_root_path())
    project_source_parent_path = root_path.joinpath("../").resolve()
    vcpkg_path = project_source_parent_path.joinpath("vcpkg").resolve()
    vcpkg_packages_list = defines.VCPKG_PACKAGES

    if not args.skip_aqt:
        setup_qt()

    if system() == "Windows":
        vcpkg_command = "vcpkg.exe"
        platform_command = commands_list()
        platform_command.add("bootstrap-vcpkg.bat", vcpkg_path, False)
        vcpkg_triplet = ["x64-windows"]
    elif system() == "Darwin":
        vcpkg_command = "./vcpkg"
        platform_command = commands_list()
        platform_command.add("chmod +x bootstrap-vcpkg.sh", vcpkg_path)
        platform_command.add("./bootstrap-vcpkg.sh", vcpkg_path, False)
        platform_command.add("chmod +x vcpkg", vcpkg_path)
        vcpkg_triplet = ["x64-osx", "arm64-osx"]
    elif system() == "Linux":
        vcpkg_command = "./vcpkg"
        platform_command = commands_list()
        platform_command.add("chmod +x bootstrap-vcpkg.sh", vcpkg_path)
        platform_command.add("./bootstrap-vcpkg.sh", vcpkg_path, False)
        platform_command.add("chmod +x vcpkg", vcpkg_path)
        vcpkg_triplet = ["x64-linux"]
    else:
        raise NotImplementedError("Unknown system: {}".format(system()))

    print(f"Clone into {vcpkg_path}")
    util.execute("git clone https://github.com/microsoft/vcpkg.git vcpkg",
            project_source_parent_path, True)
    util.execute("git fetch", vcpkg_path)
    util.execute(f"git checkout {defines.VCPKG_VERSION}", vcpkg_path)

    # Setup vcpkg via boostrap script first
    platform_command.execute_commands()  # Execute platform specific commands.

    util.execute(f"{vcpkg_command} remove --outdated --recurse", vcpkg_path, False)

    for triplet in vcpkg_triplet:
        vcpkg_packages = " ".join(vcpkg_packages_list)
        util.execute(
            f"{vcpkg_command} install {vcpkg_packages} --triplet {triplet} --recurse", vcpkg_path, False)


if __name__ == "__main__":
    main()
