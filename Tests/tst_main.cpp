#include "App/App.h"
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQuickWindow>
#include <QtQml/qqmlextensionplugin.h>
#include <QtTest>

Q_IMPORT_QML_PLUGIN(AppPlugin)

class AppTest : public QObject {
    Q_OBJECT

private slots:
    void initTestCase()
    {
        m_window = qobject_cast<QQuickWindow*>(app.engine().rootObjects().first());
        QVERIFY(m_window);
        QVERIFY(QTest::qWaitForWindowExposed(m_window));
    }

    void buttonTest();

private:
    QQuickWindow* m_window = nullptr;
    QQuickItem* m_installedSidebar = nullptr;
    App app;
};

/*!
 *  For some reason a direct findChild does not work for item
 *  delegates.
 *  https://stackoverflow.com/questions/36767512/how-to-access-qml-listview-delegate-items-from-c
 *
 */
QQuickItem* findItemDelegate(QQuickItem* listView, const QString objectName)
{
    if (!listView->property("contentItem").isValid())
        return {};

    auto contentItem = listView->property("contentItem").value<QQuickItem*>();
    auto contentItemChildren = contentItem->childItems();
    QQuickItem* videoImportConvertButton {};
    for (auto childItem : contentItemChildren) {
        if (childItem->objectName() == objectName)
            return childItem;
    }
    return {};
}

void clickItem(QQuickItem* item, Qt::MouseButton button = Qt::LeftButton)
{
    QQuickWindow* itemWindow = item->window();
    QVERIFY(itemWindow);
    auto center = item->mapToScene(QPoint(item->width() / 2, item->height() / 2)).toPoint();
    qInfo() << "click_:" << center;
    QTest::mousePress(itemWindow, button, Qt::NoModifier, center);
    QTest::qWait(50);
    QTest::mouseRelease(itemWindow, button, Qt::NoModifier, center);
    QTest::qWait(50);
}



void AppTest::buttonTest()
{
    auto* btnClickMe = m_window->findChild<QQuickItem*>("btnClickMe");
    QVERIFY(btnClickMe);
    QTest::qWait(1000);
    clickItem(btnClickMe);
    QTest::qWait(1000);
}

QTEST_MAIN(AppTest)

#include "tst_main.moc"
